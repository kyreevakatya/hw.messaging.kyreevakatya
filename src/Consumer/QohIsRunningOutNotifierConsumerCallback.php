<?php

declare(strict_types=1);

namespace App\Consumer;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;

/**
 * @author Vitaliy Bilotil <bilotilvv@gmail.com>
 */
class QohIsRunningOutNotifierConsumerCallback implements ConsumerInterface
{
    /** @var LoggerInterface */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @return int|bool One of ConsumerInterface::MSG_* constants according to callback outcome, or false otherwise.
     */

    public function execute(AMQPMessage $msg)
    {
        $this->logger->info(
            'Received message',
            [
                'message_body_raw' => $msg->getBody(),
                'routing_key' => $msg->getRoutingKey(),
            ]
        );

        $msg = json_decode($msg->getBody(), true);

        if (isset($msg['values']['qoh'], $msg)) {
            if ($msg['previous_values']['qoh'] >= 5 && $msg['values']['qoh'] < 5) {
                mail('stock@example.com', 'qoh updated', 'qoh for an inventory item dips below 5, with the current qoh.');

            }
        }

        return ConsumerInterface::MSG_ACK;
    }
}
